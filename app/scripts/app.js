'use strict';

angular
  .module('moviesApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',    
    'ui.bootstrap'    
	
  ])
 
  
  .config(function ($routeProvider) {
    $routeProvider
      
      .when('/movies/:id', {
        templateUrl: 'views/movies.html',
        controller: 'MoviesCtrl'
      })
      .when('/audios', {
        templateUrl: 'views/audios.html',
        controller: 'AudiosCtrl'
      })

      .when('/home', {
        templateUrl: 'views/home.html',
        controller: 'HomeCtrl'
      })
      
	  .when('/', {
		redirectTo: '/home'
	  })
      
      .otherwise({
        templateUrl: 'views/ikkefunnet.html',
        controller: 'IkkefunnetCtrl'
      });
  });
