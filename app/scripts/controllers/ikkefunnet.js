'use strict';

/**
 * @ngdoc function
 * @name spaCenApp.controller:IkkefunnetCtrl
 * @description
 * # IkkefunnetCtrl
 * Controller of the spaCenApp
 */
angular.module('spaCenApp')
  .controller('IkkefunnetCtrl', ['$scope', '$location', function ($scope, $location) {
    $scope.rute = $location.path();
  }]);
