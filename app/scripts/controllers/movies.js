'use strict';

/**
 * @ngdoc function
 * @name moviesApp.controller:MoviesCtrl
 * @description
 * # MoviesCtrl
 * Controller of the spaCenApp
 */
angular.module('moviesApp')
  .controller('MoviesCtrl', ['$scope', '$http', '$location', '$routeParams', '$sce', function ($scope, $http, $location, $routeParams, $sce) {
    
    $scope.params = {
        id:$routeParams.id
    }
    
    
    
    $scope.yearList = [];    
    $scope.alphabetList = [];
    
    $scope.getClass = function (year) {
    return {
      blue: ($scope.params.id).substring(1) === year || $scope.params.id === year
    };
  };
  
  $scope.sortYearAndTitle = function(resultat){
    for(var i=0;i<resultat.length;i++)
    {
        if ($scope.yearList.indexOf(resultat[i].meta.releaseYear)<0) {
            $scope.yearList.push(resultat[i].meta.releaseYear);
        }
        if ($scope.alphabetList.indexOf(resultat[i].title.substring(0,1))<0) {
            $scope.alphabetList.push(resultat[i].title.substring(0,1));
        }
    }
     $scope.yearList.sort();
     $scope.alphabetList.sort(); 
  }
  
  $scope.showOnlyYear = function(year, data){
    var mov = [];
    for(var i=0;i<data.length;i++){
        if (data[i].meta.releaseYear===year) {
            mov.push(data[i]);
        }
    }
    $scope.data = mov;
    $scope.movies = $scope.data[0];
    $scope.movie.tab1=true;
    $scope.movie.tab2=false;
    $scope.showClick=true;
  }
  
  $scope.showOnlyAlphabet = function(data){
    var movieList = [];
    for(var i=0;i<data.length;i++){
        if (data[i].title.substring(0,1)===$scope.params.id) {
            movieList.push(data[i]);
        }
    }
     $scope.data = movieList;
     $scope.movies = $scope.data[0];
     $scope.movie.tab1=false;
     $scope.movie.tab2=true;
     $scope.showClick=true;
     
  }
  
    
    $scope.movie = {
        feilet:false,
        tab1:false,
        tab2:true,
    }
    
    $scope.trustSrc = function(src) {
        return $sce.trustAsResourceUrl(src);
    }
        
    $scope.start = function(){
        $http.get('movies.json').success(
            function (resultat) {
              var act="";
              var dir="";
              
              $scope.data =  resultat;
              $scope.movie.tab1=true;
              $scope.showClick=false;
              if (($scope.params.id).indexOf("Y")>=0){
                $scope.showOnlyYear(($scope.params.id).substring(1),resultat);
              }
              else
              if (isNaN($scope.params.id)) {
               $scope.showOnlyAlphabet(resultat);
              }
              else
              $scope.movies = resultat[ $scope.params.id - 1];
              $scope.sortYearAndTitle(resultat);
              for(var i=0;i<$scope.movies.meta.actors.length;i++)
              {
                if (i==$scope.movies.meta.actors.length-1) {
                   act = act + $scope.movies.meta.actors[i].name;  
                }
                else
                act = act + $scope.movies.meta.actors[i].name + " , ";               
              }
               $scope.curAct = act;
              for(var j=0;j<$scope.movies.meta.directors.length;j++)
              {
                if (j==$scope.movies.meta.directors.length-1) {
                    dir = dir + $scope.movies.meta.directors[j].name;
                }
                else
                dir = dir + $scope.movies.meta.directors[j].name + " , ";
              }
             $scope.curDir = dir;
              
            }
           
        ).error(
			function () {
				$scope.movie.feilet = true;
				$scope.movie.feilmelding = "Ikke kontakt med movies.json";
			}
		);
    }
    
     $scope.start(); 
    
  }]);
  
  angular.module('moviesApp').filter('trusted', ['$sce', function ($sce) {
    return function(url) {
        return $sce.trustAsResourceUrl(url);
    };
}]);
  